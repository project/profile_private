<?php

namespace Drupal\profile_private\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;
use Drupal\Core\Session\AccountProxyInterface;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * RedirectAnonymousSubscriber constructor.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   CurrentUser.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $current_route_match
   *   CurrentRoute.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config.
   * @param \Drupal\Core\Routing\RouteProvider $routeProvider
   *   Route.
   */
  public function __construct(AccountProxyInterface $current_user) {
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // Check if the user is logged in.
    if ($this->currentUser->isAuthenticated()) {
      // Define access for '/user/{user}'.
      if ($route = $collection->get('entity.user.canonical')) {
        $route->setRequirement('_custom_access', '\Drupal\profile_private\Access\ProfileAccessCheck::access');
      }
    }
  }

}
