<?php

namespace Drupal\profile_private\Access;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;

/**
 * Checks access for displaying user profile page.
 */
class ProfileAccessCheck implements AccessInterface {

  /**
   * A profile access check.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account, $user) {

    // Get the private access.
    $db_conf = \Drupal::service('profile_private.db_conf');
    $privacy = $db_conf->getPrivateAccess($user);

    // Check if admin has "Administer users" permission.
    return AccessResult::allowedIfHasPermission($account, 'administer users')
      // Check if current id privacy.
      ->orIf(AccessResult::allowedIf($privacy == 0))
      // Check if current visited user id = user id.
      ->orIf(AccessResult::allowedIf($user == $account->id()));
  }

}
