<?php

namespace Drupal\profile_private;

use Drupal\Core\Database\Connection;

/**
 * Defines handles the profile grants system.
 *
 * This is used to check profile access.
 */
class PrivateDbConfig {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs a PrivateDbConfig object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * Add new/update record in table profile_private.
   */
  public function merge($uid, $private) {
    // Insert / Update profile private.
    $query = $this->database->merge('profile_private');
    $query->insertFields([
      'uid' => $uid,
      'private' => $private,
    ]);
    $query->updateFields([
      'uid' => $uid,
      'private' => $private,
    ]);
    $query->key(['uid' => $uid]);
    return $query->execute();
  }

  /**
   * Get records by uid from table profile_private.
   */
  public function getPrivateAccess($uid) {
    $query = $this->database->select('profile_private', 'p');
    $query->fields('p', ['private']);
    $query->condition('p.uid', $uid, '=');
    $result = $query->execute()->fetchField();
    return $result;
  }

}
